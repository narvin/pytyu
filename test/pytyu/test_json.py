"""Tests for the json_schema module."""

from collections.abc import Mapping, Sequence
import logging

import pytest

from pytyu.json import is_json


class TestIsJson:
    """Test suite for is_json function."""

    @staticmethod
    @pytest.mark.parametrize("value", (None, 1, "foo", {1: 1}, {"foo": str}, []))
    def test_fails_invalid_objects(value: object) -> None:
        """Failure cases."""
        assert not is_json(value)

    @staticmethod
    @pytest.mark.parametrize(
        "value",
        (
            {},
            {"foo": "bar"},
            {"foo": {}},
            {"foo": []},
            {
                "str": "s",
                "int": 1,
                "float": 1.1,
                "bool": True,
                "none": None,
                "dict": {"foo": "bar"},
                "list": ["str", {"foo": "bar"}],
            },
        ),
    )
    def test_passes_valid_objects(value: object) -> None:
        """Success cases."""
        assert is_json(value)

    @staticmethod
    @pytest.mark.parametrize(
        "value, exp_logs",
        (
            ("foo", ({"func": "is_json", "args": ("foo", False)},)),
            (
                {"foo": "bar"},
                ({"func": "is_json", "args": ({"foo": "bar"}, True)},),
            ),
            (
                {"foo": 1, "bar": 2},
                ({"func": "is_json", "args": ({"foo": 1, "bar": 2}, True)},),
            ),
            (
                {"foo": ["bar", 1]},
                ({"func": "is_json", "args": ({"foo": ["bar", 1]}, True)},),
            ),
            (
                {"foo": {"bar": 1}},
                (
                    {"func": "is_json", "args": ({"bar": 1}, True)},
                    {"func": "is_json", "args": ({"foo": {"bar": 1}}, True)},
                ),
            ),
        ),
    )
    def test_logging(
        value: object,
        exp_logs: Sequence[Mapping[str, object]],
        caplog: pytest.LogCaptureFixture,
    ) -> None:
        """Log messages."""
        caplog.set_level(logging.DEBUG)
        is_json(value)
        assert len(caplog.records) == len(exp_logs)
        for log_record, exp_log in zip(caplog.records, exp_logs):
            assert log_record.levelname == "DEBUG"
            assert log_record.msg == "\n\tvalue=%s\n\tres=%s"
            assert log_record.funcName == exp_log["func"]
            assert log_record.args == exp_log["args"]

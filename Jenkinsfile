pipeline {
  agent none

  stages {
    stage('pre-build') {
      steps {
        updateGitlabCommitStatus state: 'pending'
      }
    }

    stage('check-test') {
      parallel {
        stage('check') {
          agent {label 'py310-checking'}
          steps {
            sh '''
              make install-check
              make -k check
            '''
          }
        }
        stage('test') {
          agent {label 'py3-supported'}
          steps {
            sh 'make test-all'
          }
        }
      }
    }

    stage('build-only') {
      agent {label 'py3-build'}
      when {
        not {
          branch 'main'
        }
      }
      steps {
        sh 'make build'
      }
    }

    stage('build-deploy') {
      agent {label 'py3-build'}
      when {
        branch 'main'
      }
      steps {
        withCredentials([usernamePassword(credentialsId: 'pypi-personal-prod-token', passwordVariable: 'TWINE_PASSWORD', usernameVariable: 'TWINE_USERNAME')]) {
            sh 'make deploy'
        }
      }
    }
  }

  post {
    success {
      updateGitlabCommitStatus state: 'success'
    }
    unsuccessful {
      updateGitlabCommitStatus state: 'failed'
    }
  }
}
